#!/usr/bin/env python3
'''
Decrypt and print smart meter telegrams.
Automatically update when new data is present at the datafile.

Usage
./monitor.py path/to/logfile ENCRYPTIONKEY

Notes:
This script does not fully sanitize input. 3 phase data is expected
'''
import os
import sys
import time
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler
import decryptor


class DataType:
    NullData = 0x00
    Boolean = 0x03
    BitString = 0x04
    DoubleLong = 0x05
    DoubleLongUnsigned = 0x06
    OctetString = 0x09
    VisibleString = 0x0A
    Utf8String = 0x0C
    BinaryCodedDecimal = 0x0D
    Integer = 0x0F
    Long = 0x10
    Unsigned = 0x11
    LongUnsigned = 0x12
    Long64 = 0x14
    Long64Unsigned = 0x15
    Enum = 0x16
    Float32 = 0x17
    Float64 = 0x18
    DateTime = 0x19
    Date = 0x1A
    Time = 0x1B
    Array = 0x01
    Structure = 0x02
    CompactArray = 0x13


class Obis:
    '''
    class obis

    Parse OBIS object
    '''

    def __init__(self):
        #self.thistime = TimeObj()
        self.obis = {}
        self.userinput = ""
        self.year = 0
        self.month = 0
        self.day = 0
        self.day_of_week = 0
        self.hour = 0
        self.minute = 0
        self.second = 0

    def add_inputdata(self, inputdata):
        self.userinput = inputdata

    # convert obis string into hex string
    def to_bytes(code):
        return bytes([int(a) for a in code.split(".")])

    # OBIS codes
    VoltageL1 = to_bytes("1.0.32.7.0.255") # value * 10^-1 [V]
    VoltageL2 = to_bytes("1.0.52.7.0.255")
    VoltageL3 = to_bytes("1.0.72.7.0.255")
    CurrentL1 = to_bytes("1.0.31.7.0.255") # value * 10^-2 A, unsigned
    CurrentL2 = to_bytes("1.0.51.7.0.255")
    CurrentL3 = to_bytes("1.0.71.7.0.255")
    RealPowerIn = to_bytes("1.0.1.7.0.255") # current power from the grid
    RealPowerOut = to_bytes("1.0.2.7.0.255") # current power to the grid
    RealEnergyIn = to_bytes("1.0.1.8.0.255") # absolute energy from the grid
    RealEnergyOut = to_bytes("1.0.2.8.0.255") # absolute energy to the grid
    PowerFactor = to_bytes("1.0.13.7.0.255") # power factor, value * 10^-3

    # not present at EVN
    ReactivePowerIn = to_bytes("1.0.3.7.0.255")
    ReactivePowerOut = to_bytes("1.0.4.7.0.255")
    ReactiveEnergyIn = to_bytes("1.0.3.8.0.255")
    ReactiveEnergyOut = to_bytes("1.0.4.8.0.255")


    def parse_all(self):
        decrypted = self.userinput

        #TODO fix extraction of the timstamp
        # there are a lot of edgecases which are currently not properly handled
        # maybe this could be converted into a datetime object, thic could make conversion easier
        # or use Gurux framework
        # further ressources:
        # https://www.openmuc.org/dlms-cosem/javadoc/org/openmuc/jdlms/datatypes/CosemDateTime.html
        # https://github.com/Gurux/GuruxDLMS.c/blob/master/development/src/date.c
        # http://www.fusdom.com/upload/file/xx/DLMS_Blue_Book_Identification_System.pdf

        timestamp = decrypted[6:18] #assuming at fixed position in decrypted telegram

        self.year = int.from_bytes(timestamp[0:2], "big")
        self.month = int.from_bytes(timestamp[2:3], "big")
        self.day = int.from_bytes(timestamp[3:4], "big")
        self.day_of_week = int.from_bytes(timestamp[4:5], "big")
        self.hour =int.from_bytes(timestamp[5:6], "big")
        self.minute = int.from_bytes(timestamp[6:7], "big")
        self.second = int.from_bytes(timestamp[7:8], "big")
        #print("hundreths  ", int.from_bytes(timestamp[8:9], "big"))
        #print("dev ", int.from_bytes(timestamp[9:11], "big"))

        # decoding mechanism from https://github.com/tirolerstefan/kaifa
        pos = 0
        total = len(decrypted)
        self.obis = {}
        while pos < total:
            if decrypted[pos] != DataType.OctetString:
                pos += 1
                continue
            if decrypted[pos + 1] != 6:
                pos += 1
                continue
            obis_code = decrypted[pos + 2 : pos + 2 + 6]
            data_type = decrypted[pos + 2 + 6]
            pos += 2 + 6 + 1

            #print("OBIS code {} DataType {}".format(binascii.hexlify(obis_code),data_type))
            if data_type == DataType.DoubleLongUnsigned:
                value = int.from_bytes(decrypted[pos : pos + 4], "big")
                scale = decrypted[pos + 4 + 3]
                if scale > 128:
                    scale -= 256
                pos += 2 + 8
                self.obis[obis_code] = value*(10**scale)
                #print("DLU: {}, {}, {}".format(value, scale, value*(10**scale)))
            elif data_type == DataType.LongUnsigned:
                value = int.from_bytes(decrypted[pos : pos + 2], "big")
                scale = decrypted[pos + 2 + 3]
                if scale > 128:
                    scale -= 256
                pos += 8
                self.obis[obis_code] = value*(10**scale)
                #print("LU: {}, {}, {}".format(value, scale, value*(10**scale)))
            elif data_type == DataType.OctetString:
                octet_len = decrypted[pos]
                octet = decrypted[pos + 1 : pos + 1 + octet_len]
                pos += 1 + octet_len + 2
                self.obis[obis_code] = octet
                #print("OCTET: {}, {}".format(octet_len, octet))


    def print_phase_and_total_power(self):
        '''
        test
        '''
        # print phases in W with two decimal places
        print("{}-{:02d}-{:02d}_{:02d}:{:02d}:{:02d}".format(self.year,
                                                             self.month,
                                                             self.day,
                                                             self.hour,
                                                             self.minute,
                                                             self.second))

        print("Phase 1: " + "{:.2f}".format(self.obis[Obis.VoltageL1]*self.obis[Obis.CurrentL1]) + "W")
        print("Phase 2: " + "{:.2f}".format(self.obis[Obis.VoltageL2]*self.obis[Obis.CurrentL2]) + "W")
        print("Phase 3: " + "{:.2f}".format(self.obis[Obis.VoltageL3]*self.obis[Obis.CurrentL3]) + "W")
        print("Power from Grid: " + "{}".format(self.obis[Obis.RealPowerIn]) + "W")
        print("Power to Grid: " + "{}".format(self.obis[Obis.RealPowerOut]) + "W")
        print("Power Factor {:.2f}".format(self.obis[Obis.PowerFactor]))


    def print_raw_OBIS(self):
        print("Raw OBIS Values:")
        for e in self.obis:
            print("{}.{}.{}.{}.{}.{}: {}".format(e[0],e[1],e[2],e[3],e[4],e[5], self.obis[e]))


class UpdateHandler(FileSystemEventHandler):
    '''
    Process input file, decrypt via library, print to console
    '''
    def __init__(self):
        self.counter = 0
        self.file = ""
        self.key = ""
        self.last_line = ""

    def get_last_line(self):
        '''
        Use file seeking to handle  large files
        Relies upon line break \n
        '''
        try:
            self.file.seek(-2, os.SEEK_END)
            while self.file.read(1) != b'\n':
                self.file.seek(-2, os.SEEK_CUR)
        except OSError:
            self.file.seek(0)
        self.last_line = self.file.readline().decode().strip()

    def on_modified(self, event):
        '''
        Ignore when triggered by the directory change
        '''
        if os.path.isdir(event.src_path):
            return
        self.process_data()

    def process_data(self):
        '''
        Decrypt and print the data to console
        '''
        decobj = decryptor.DlmsDecryptor()

        self.get_last_line()
        ret = decobj.decrypt(bytes.fromhex(self.last_line), self.key)

        if ret is not False:
            OBIS_obj = Obis()
            OBIS_obj.add_inputdata(bytes.fromhex(ret))
            OBIS_obj.parse_all()

            os.system('clear')
            OBIS_obj.print_phase_and_total_power()
            print("")
            OBIS_obj.print_raw_OBIS()
        else:
            print("Error decrypting")


if __name__ == "__main__":

    if len(sys.argv) != 3:
        print("Usage: monitor.py path/to/datafile.txt HEXKEYSTRING")
        sys.exit(10)

    # check if key length is correct
    if len(sys.argv[2]) != 32:
        print("length of keystring is wrong, should be 32, but {} was entered".format(len(sys.argv[2])))
        sys.exit(20)

    # check if key is a valid hex string
    for char in sys.argv[2]:
        if char not in "0123456789abcdefABCDEF":
            print("One or more elements of the key string do not represent a hex value: {}".format(char))
            sys.exit(30)

    try:
        with open(sys.argv[1], "rb") as f:
            event_handler = UpdateHandler()
            event_handler.file = f
            event_handler.key = sys.argv[2]
            event_handler.process_data()

            observer = Observer()
            observer.schedule(event_handler, path=sys.argv[1], recursive=False)
            observer.start()

            try:
                while True:
                    time.sleep(1)
            except KeyboardInterrupt:
                observer.stop()
            observer.join()

    except Exception as err:
        print("Failed to open datafile: ", err)

# EVN Smart Meter Logger

Log and view EVN Smart Meter Messages via M-Bus (Meter Bus).
The primary idea is to log the raw data from the smart meter to allow post processing.

Supported Models
* Kaifa MA309M
* Sagemcom T210-D

## Scripts

**evn_smart_meter_logger.py**

Logs the raw (encrypted) telegrams from the smart meter to a file.<br>
This can be done without the decryption key and allows for maximum possible further processing.<br>
Script can run via Systemd unit for long term logging.<br>
A encrypted telegram every 5 seconds (~565 bytes) will accumulate to ~1.5GB per year.

**monitor.py**

Decrypt and view the last telegram from the data logged by the evn_smart_meter_logger.py.<br>
Auto update when new data is present.S

## Setup of logger
### Hardware needed

* M-BUS slave to USB adapter
* RJ11/RJ12 cable (only the center two conductors are needed for M-Bus)
* PC running Python, preferably Linux

### Logger setup
1. Clone this repository (e.g. to your local home folder)

        $ git clone https://gitlab.com/neo0x3d/evn-smart-meter-logger

1. Edit the config file (meter_config.json) if needed.

1. Test run the script to verify functionality

        $ ./evn_smart_meter_logger.py

1. Copy the systemd unit, set the absolute paths for the Python script and WorkingDirectory

        # cp ./systemd/evn-smart-meter-logger@.service /usr/lib/systemd/system/

1. Try to start the systemd unit, if successful enable it.<br>
Replace USER with the user that the script should run as.

        # systemctl daemon-reload
        # systemctl start evn-smart-meter-logger@MYUSER
        # systemctl enable evn-smart-meter-logger@MYUSER

1. Ensure everything runs reliable and check the logged data:<br>
Removing and plugging in the USB adapter should result in the systemd service restarting after 60 seconds.<br>
The systemd service should auto start on boot.<br>
The data file should contain the raw messages.


### Hints
* Default config should be sufficient to run.
* The script should run as a user, there is no need to run it as root if the permissions for the USB device are set properly.
* It is expected that the USB M-Bus adapter enumerates to the same (/dev/ttyUSB0) device, when multiple devices are present this might cause conflicts. A udev rule could fix this if needed.

### Start monitor script

    $ ./monitor.py path/to/datafile.txt ENCRYPTIONKEY


## EVN / Austrian Smart Meter References
EVN Kundenschnittstelle Referenz https://www.netz-noe.at/Download-(1)/Smart-Meter/218_9_SmartMeter_Kundenschnittstelle_lektoriert_14.aspx

Available Smart Meter customer interfaces in Austria https://oesterreichsenergie.at/fileadmin/user_upload/Smart_Meter-Plattform/20200201_Konzept_Kundenschnittstelle_SM.pdf
## Credits
Parts of this project (obis parsing, decrypting) were derived from https://github.com/tirolerstefan/kaifa

#!/usr/bin/env python3
'''
DLMS/COSEM decryptor

Supported standards:
DLMS Security Suite 0, HLS5

'''
import sys
import re
import binascii
from Cryptodome.Cipher import AES


class Supplier:
    name = None
    frame1_start_bytes_hex = '68fafa68'
    frame1_start_bytes = b'\x68\xfa\xfa\x68'  # 68 FA FA 68
    ic_start_byte = None
    enc_data_start_byte = None


class SupplierTINETZ(Supplier):
    name = "TINETZ"
    frame2_start_bytes_hex = '68727268'
    frame2_start_bytes = b'\x68\x72\x72\x68'  # 68 72 72 68
    ic_start_byte = 23
    enc_data_start_byte = 27


class SupplierEVN(Supplier):
    name = "EVN"
    frame2_start_bytes_hex = '68141468'
    frame2_start_bytes = b'\x68\x14\x14\x68'  # 68 14 14 68
    ic_start_byte = 22
    enc_data_start_byte = 26


class DlmsDecryptor:
    '''
    Decryption mechanism derived from https://github.com/tirolerstefan/kaifa
    '''

    def __init__(self):
        #TODO implement autodetect mechanism
        self.g_supplier = SupplierEVN()

    def decrypt(self, ciphertext, key):
        frame1 = b''
        frame2 = b''

        frame1_start_pos = -1
        frame2_start_pos = -1

        # try to find frames in received data
        frame1_start_pos = ciphertext.find(self.g_supplier.frame1_start_bytes)
        frame2_start_pos = ciphertext.find(self.g_supplier.frame2_start_bytes)

        # only continue if frame 1 and 2 are found
        if (frame1_start_pos != -1) and (frame2_start_pos != -1):

            #TODO this mechanism should be simplified since we don't expect random data
            regex = binascii.unhexlify('28'+self.g_supplier.frame1_start_bytes_hex+'7c'+self.g_supplier.frame2_start_bytes_hex+'29')
            l = re.split(regex, ciphertext)
            l = list(filter(None, l))  # remove empty elements

            for i, el in enumerate(l):
                if el == self.g_supplier.frame1_start_bytes:
                    frame1 = l[i] + l[i+1]
                    frame2 = l[i+2] + l[i+3]
                    break

            # check for weird result -> exit
            if (len(frame1) == 0) or (len(frame2) == 0):
                print("Frame1 or Frame2 is empty: {} | {}".format(frame1, frame2))
                return False

            #print("TELEGRAM1:\n{}\n".format(binascii.hexlify(frame1)))
            #print("TELEGRAM2:\n{}\n".format(binascii.hexlify(frame2)))

        else:
            print("frame 1 or frame 2 not found, cannot decrypt")
            return False


        #print("Decrypt: FRAME1:\n{}".format(binascii.hexlify(frame1)))
        #print("Decrypt: FRAME2:\n{}".format(binascii.hexlify(frame2)))
        AESkey = binascii.unhexlify(key)  # convert to binary stream
        systitle = frame1[11:19]  # systitle at byte 12, length 8
        #print("SYSTITLE: {}".format(binascii.hexlify(systitle)))
        ic = frame1[self.g_supplier.ic_start_byte:self.g_supplier.ic_start_byte+4]   # invocation counter length 4
        #print("IC: {} / {}".format(binascii.hexlify(ic), int.from_bytes(ic,'big')))
        iv = systitle + ic   # initialization vector
        #print("IV: {}".format(binascii.hexlify(iv)))
        data_frame1 = frame1[self.g_supplier.enc_data_start_byte:len(frame1) - 2]  # start at byte 26 or 27 (dep on supplier), excluding 2 bytes at end: checksum byte, end byte 0x16
        data_frame2 = frame2[9:len(frame2) - 2]   # start at byte 10, excluding 2 bytes at end: checksum byte, end byte 0x16
        #print("DATA FRAME1\n{}".format(binascii.hexlify(data_frame1)))
        #print("DATA FRAME1\n{}".format(binascii.hexlify(data_frame2)))
        data_encrypted = data_frame1 + data_frame2
        cipher = AES.new(AESkey, AES.MODE_GCM, nonce=iv)
        data_decrypted = cipher.decrypt(data_encrypted)
        data_decrypted_hex = binascii.hexlify(data_decrypted)

        # return decrypted data as string
        return data_decrypted_hex.decode('utf-8')

#!/usr/bin/env python3
import sys
import binascii
import logging
import json
import serial


CONFIGFILE = "./meter_config.json"


if __name__ == '__main__':

    # load config file
    try:
        with open(CONFIGFILE, "r") as cfg_file:
            config = json.load(cfg_file)
    except Exception as err:
        print("Error loading config file ", err)
        sys.exit(10)


    # initialize logging
    try:
        logFormatter = logging.Formatter("%(asctime)s [%(levelname)s]:  %(message)s")
        log = logging.getLogger()
        log.setLevel(eval(config['loglevel'])) # convert logging.DEBUG into int

        fileHandler = logging.FileHandler(config['logfile'])
        fileHandler.setFormatter(logFormatter)
        log.addHandler(fileHandler)

        if config['consoleoutput']:
            consoleHandler = logging.StreamHandler()
            consoleHandler.setFormatter(logFormatter)
            log.addHandler(consoleHandler)
    except Exception as err:
        print("Error starting logger ", err)
        sys.exit(20)


    # open serial port
    try:
        ser = serial.Serial(config['port'], config['baudrate'], timeout=config['timeout'])
    except Exception as err_serial:
        log.error("Error opening serial port: %s",err_serial)
        sys.exit(30)


    # main aquisition loop
    while True:
        INPUTBUF = b""

        # assuming we read binary data of not exactly known length
        # aquisition stops via triggering of the timeout of read()
        while True:
            c = ser.read()
            if len(c) == 0:
                log.debug("finished search")
                break
            INPUTBUF += c

        # lengh seems to be always 282,
        # 250-300 were set as limits to accomodate for small variations.
        # unclear if telegram size is different for single phase meter
        if len(INPUTBUF) < 250 or len(INPUTBUF) > 300:
            log.debug("insufficient length: %d",len(INPUTBUF))
            continue

        if len(INPUTBUF) != 282:
            log.error("size != 282 found but within valid range: %d", len(INPUTBUF))

        # only accept telegrams with the proper start sequence
        if not INPUTBUF.startswith(b"\x68\xfa\xfa\x68"):
            log.error("wrong start, input data: %s", binascii.hexlify(INPUTBUF))
            continue

        log.debug("sufficient data found: %s", binascii.hexlify(INPUTBUF))


        try:
            with open(config['exportfile'], "a") as f:
                f.write(binascii.hexlify(INPUTBUF).decode("utf-8") + "\n")
                log.debug("data written to file")
        except Exception as err:
            log.error("Error writing to file %s %s", f, err)
